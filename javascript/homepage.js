
var headertext = "I'm Colin McGravey, a junior at the University of Michigan studying computer science and entrepreneurship. This website provides a closer look into my previous experience, resume, and current and past projects, and also gives me a place to share the material that I have found thought-provoking and worthwhile. Check out the info below to learn more about me!";
var universitybutton = "University Experience";
var workbutton = "Work Experience";
var resumebutton = "Click here for access to my full resume!"

var linkedin_link = "https://www.linkedin.com/in/colin-mcgravey/";
var codeberg_link = "https://codeberg.org/colinmcgravey";
var resume_link = "https://drive.google.com/drive/u/0/folders/0AID65usrGF3-Uk9PVA"; 

// headshot background
var imgbgx = 90; 
var imgbgy = 90; 
var imgbgwidth = 307;
var imgbgheight = 331;

// headshot image 
var imgx = 100;
var imgy = 100; 

// logo image
var logox = 55;
var logoy = 440;
var logowidth = 377;
var logoheight = 80;

// link rectangles 
var linkedinx = 140;
var linkediny = 550; 

var codebergx = 280; 
var codebergy = 550; 

var linkwidth = 60;
var linkheight = 60;

// main text rectangle 
var textx = 500
var texty = 90

// button text rectangles 
var button1x = 520
var button2x = 840
var buttony = 300

var button3x = 520;
var button3y = 400;

// max rect dims 
var max_rect_width = 1000; 
var max_rect_height = 420; 
var rect_width = 0; 
var rect_height = 0; 

// x button dims
var x_button_x = 90; 
var x_button_y = 50; 
var x_buttonw = 50; 
var x_buttonh = 54; 


var windowopen = false; 
var windowclose = true; 
var windowtype = 0; 

// background lines
var spacing = 40; 
class Lines {
    constructor(x, y, x2, y2) {
        this.x = x; 
        this.y = y; 
        this.x2 = x2;
        this.y2 = y2; 
    }
}
var linearr = []; 


let x = 0; 
let y = 0; 

function preload() {
    headshot = loadImage("javascript/images/head.png");
    logo = loadImage("javascript/images/website_logo.png");
    linkedin = loadImage("assets/linkedin.png"); 
    codeberg = loadImage("assets/codeberg1.png"); 
    universitybutton = loadImage("assets/sitebutton-01.png");
    workbutton = loadImage("assets/sitebutton-02.png"); 
    resumebutton = loadImage("assets/sitebutton3.png");
    xicon = loadImage("javascript/images/xbutton.png"); 
    universityexp = loadImage("javascript/images/universityexp.png"); 
    workexp = loadImage("javascript/images/workexp.png"); 
}

function setup() {
    createCanvas(1240, 640);
}

function new_line(x, y) { 
    if (random(1) > 0.5) {
        var line = new Lines(x, y, x+spacing, y+spacing);
        append(linearr, line);
    }
    else {
        var line = new Lines(x, y+spacing, x+spacing, y);
        append(linearr, line);
    }
}

function mousePressed() {
    if (linkedinx < mouseX && mouseX < linkedinx + linkwidth) {
        if (linkediny < mouseY && mouseY < linkediny + linkheight) {
          window.open(linkedin_link); 
        }
    } 
    else if (codebergx < mouseX && mouseX < codebergx + linkwidth) {
        if (codebergy < mouseY && mouseY < codebergy + linkheight) {
            window.open(codeberg_link);
        }
    }
    else if (button1x < mouseX && mouseX < button1x + 280) {
        if (buttony < mouseY && mouseY < buttony + 60) {
            windowopen = true; 
            windowclose = false; 
            windowtype = 1; 
        }
    }
    else if (button2x < mouseX && mouseX < button2x + 280) {
        if (buttony < mouseY && mouseY < buttony + 60) {
            windowopen = true; 
            windowclose = false; 
            windowtype = 2; 
        }
    }
    else if (button3x < mouseX && mouseX < button3x + 620) {
        if (button3y < mouseY && mouseY < button3y + 90) {
            window.open(resume_link); 
        }
    }
    if (windowopen) {
        if (x_button_x < mouseX && mouseX < x_button_x + x_buttonw) {
            if (x_button_y < mouseY && mouseY < x_button_y + x_buttonh) {
                windowopen = false; 
                windowclose = true; 
            }
        }
    }
}

function spawn_rect() {
    if (rect_height < max_rect_height) {
        rect_height += 40; 
    }
    else if (rect_width < max_rect_width) {
        rect_width += 40; 
    }
}

function destroy_rect() {
    rect_height = 0; 
    rect_width = 0; 

}

function draw() {
    // background
    background("#92E5C6");
    stroke("#1D3F5C");

    for(var i = 0; i < linearr.length; i++) {
        line(linearr[i].x, linearr[i].y, linearr[i].x2, linearr[i].y2); 
    }
    x = x + 40; 
    if (x > width) {
        x = 0; 
        y = y + spacing; 
    }
    new_line(x, y); 
    stroke(255);

    fill("#1D3F5C");

    // headshot and background
    rect(imgbgx, imgbgy, imgbgwidth, imgbgheight, 10);
    image(headshot, imgx, imgy, imgbgwidth - 20, imgbgheight - 20);

    // logo and background
    rect(logox, logoy, logowidth, logoheight); 
    image(logo, logox, logoy, logowidth, logoheight);

    // linked in and codeberg links
    rect(linkedinx, linkediny, 60, 60, 20); 
    image(linkedin, 133, 552, 75, 55); 

    rect(codebergx, codebergy, 60, 60, 20); 
    image(codeberg, 285, 555, 50, 50); 

    // main text
    rect(textx, texty, 660, 440, 20);
    fill("#577F9C"); 
    rect(button1x, buttony, 300, 70, 10);
    rect(button2x, buttony, 300, 70, 10);
    rect(button3x, button3y, 620, 90, 10);
    fill("#AFB7F7");
    textSize(22);
    text(headertext, 520, 110, 630);
    image(universitybutton, 525, 305, 280, 60);
    image(workbutton, 845, 305, 280, 60);
    image(resumebutton, 525, 405, 610, 80);
    textSize(22);

    if (windowopen) {
        cursor(ARROW);
        spawn_rect(); 
        rect(80, 40, rect_width, rect_height, 10); 
        if (rect_width >= max_rect_width) {
            image(xicon, 90, 50, 50, 54); 
            if (windowtype == 1) {
                image(universityexp, 150, 50, 900, 400);                 
            }
            else {
                image(workexp, 150, 50, 900, 400); 
            }
        }
    }
    if (windowclose) {
        destroy_rect(); 
        rect(80, 40, rect_width, rect_height, 10);
        cursor(ARROW);
    }

    if (windowclose) {
        if (linkedinx < mouseX && mouseX < linkedinx + linkwidth) {
            if (linkediny < mouseY && mouseY < linkediny + linkheight) {
            cursor(HAND); 
            }
        } 
        else if (codebergx < mouseX && mouseX < codebergx + linkwidth) {
            if (codebergy < mouseY && mouseY < codebergy + linkheight) {
                cursor(HAND); 
            }
        }
        else if (button1x < mouseX && mouseX < button1x + 280) {
            if (buttony < mouseY && mouseY < buttony + 60) {
                cursor(HAND);  
            }
        }
        else if (button2x < mouseX && mouseX < button2x + 280) {
            if (buttony < mouseY && mouseY < buttony + 60) {
                cursor(HAND); 
            }
        }
        else {
            cursor(ARROW); 
        }
    }


}