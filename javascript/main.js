

function univ_click() {
    var rmv = document.getElementById("unv-exp-button"); 
    if (rmv.textContent == "Expand") {

        // remove expand button
        var div = document.getElementById("University Experience"); 
        div.removeChild(rmv); 


        // create paragraph and append 
        var tag = assemble_unv_exp();  

        // create collapse button and set attributes 
        var tag2 = document.createElement("button");
        tag2.setAttribute("id", "unv-exp-button"); 
        tag2.setAttribute("onclick", "univ_click()"); 
        var text = document.createTextNode("Collapse"); 
        tag2.appendChild(text); 

        // append paragraph and button to div 
        div.appendChild(tag); 
        div.appendChild(tag2); 
    }
    else {

        // grab div and remove the collapse button
        var div = document.getElementById("University Experience"); 
        div.removeChild(rmv); 

        // grab paragraph and remove 
        var para = document.getElementById("unv-exp"); 
        div.removeChild(para); 


        // create expand button and set attributes
        var tag = document.createElement("button"); 
        tag.setAttribute("id", "unv-exp-button"); 
        tag.setAttribute("onclick", "univ_click()"); 
        var text = document.createTextNode("Expand"); 
        tag.appendChild(text); 
        
        // append expand button to div 
        div.appendChild(tag); 

    }
}

function assemble_unv_exp() {

    var div = document.createElement("div"); 
    div.setAttribute("id", "unv-exp"); 


    var h1 = document.createElement("h3"); 
    var h1text = document.createTextNode("EECS 280: Programming and Intro to Data Structures"); 
    h1.appendChild(h1text); 
    div.appendChild(h1); 

    var ul1 = document.createElement("ul"); 

    var li1 = document.createElement("li"); 
    var li1text = document.createTextNode("Worked Extensively with the C++ Language, and became familiar with Object-Oriented Programming techniques"); 
    li1.appendChild(li1text); 

    var li2 = document.createElement("li");
    var li2text = document.createTextNode("Learned to create a simple machine learning project, worked with Binary Search Trees and Maps"); 
    li2.appendChild(li2text); 

    var li3 = document.createElement("li"); 
    var li3text = document.createTextNode("Formulated a simple API"); 
    li3.appendChild(li3text); 

    var li4 = document.createElement("li"); 
    var li4text = document.createTextNode("Created an image-resizing algorithm"); 
    li4.appendChild(li4text); 

    ul1.appendChild(li1);
    ul1.appendChild(li2);
    ul1.appendChild(li3);
    ul1.appendChild(li4);

    div.appendChild(ul1); 





    var h11 = document.createElement("h3"); 
    var h11text = document.createTextNode("EECS 203: Discrete Mathematics"); 
    h11.appendChild(h11text); 
    div.appendChild(h11); 

    var ul2 = document.createElement("ul"); 

    var li11 = document.createElement("li"); 
    var li11text = document.createTextNode("Worked with both Natural Deduction and Mathematical Induction to construct consice and accurate proofs");
    li11.appendChild(li11text);

    var li22 = document.createElement("li"); 
    var li22text = document.createTextNode("Gained an understanding of Big O runtime complexities and how to leverage this understanding to my advantage in my programming."); 
    li22.appendChild(li22text); 
    
    ul2.appendChild(li11);
    ul2.appendChild(li22); 
    
    div.appendChild(ul2); 


    return div; 



}


function work_click() {
    var rmv = document.getElementById("work-exp-button"); 

    if (rmv.textContent == "Expand") {

        // remove expand button
        var div = document.getElementById("Work Experience"); 
        div.removeChild(rmv); 


        // create paragraph and append 
        var tag = assemble_work_exp();  

        // create collapse button and set attributes 
        var tag2 = document.createElement("button");
        tag2.setAttribute("id", "work-exp-button"); 
        tag2.setAttribute("onclick", "work_click()"); 
        var text = document.createTextNode("Collapse"); 
        tag2.appendChild(text); 

        // append paragraph and button to div 
        div.appendChild(tag); 
        div.appendChild(tag2); 
    } 

    else {
        // grab div and remove the collapse button
        var div = document.getElementById("Work Experience"); 
        div.removeChild(rmv); 

        // grab paragraph and remove 
        var para = document.getElementById("work-exp"); 
        div.removeChild(para); 


        // create expand button and set attributes
        var tag = document.createElement("button"); 
        tag.setAttribute("id", "work-exp-button"); 
        tag.setAttribute("onclick", "work_click()"); 
        var text = document.createTextNode("Expand"); 
        tag.appendChild(text); 
        
        // append expand button to div 
        div.appendChild(tag); 
    }


}

function assemble_work_exp() {
    var div = document.createElement("div"); 
    div.setAttribute("id", "work-exp");

    var h1 = document.createElement("h3"); 
    var h1text = document.createTextNode("iD Tech Camps: Full-Time Employee"); 
    h1.appendChild(h1text);
    div.appendChild(h1); 


    var ol = document.createElement("ul"); 
    

    var li1 = document.createElement("li"); 
    var li1text = document.createTextNode("Educated students ages 13-17, beginning with the basics of the Python language, and finishing with an introduction to Tensorflow, Image Classification, and Machine Learning"); 
    li1.appendChild(li1text);
    
    
    var li2 = document.createElement("li"); 
    var li2text = document.createTextNode("Undertook projects with advanced students outside of the routine curriculum, i.e. custom Image Classification Models and a deeper look into gradient descent."); 
    li2.appendChild(li2text); 

    var li3 = document.createElement("li"); 
    var li3text = document.createTextNode("Additional time spent teaching game design both with Python and Javascript, and introduced students to Object Oriented Programming concepts."); 
    li3.appendChild(li3text); 


    var li4 = document.createElement("li"); 
    var li4text = document.createTextNode("Became very familiar with working with common data science modules in Python such as Numpy, Pandas, and Scikit-learn."); 
    li4.appendChild(li4text);

    ol.appendChild(li1); 
    ol.appendChild(li2); 
    ol.appendChild(li3); 
    ol.appendChild(li4); 

    div.appendChild(ol); 


    return div; 




}