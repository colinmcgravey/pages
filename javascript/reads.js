
var maintenance = "This page is currently under maintenance, please check back soon!"; 


function setup() {
    createCanvas(1240, 640); 
}

function draw() {
    background("#92E5C6");
    fill(0); 
    textSize(26); 
    text(maintenance, 200, 100, 800); 
}