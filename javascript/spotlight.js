
// variables for width/height of canvas 
var canvasWidth, canvasHeight; 

// variables for width/height of side to side buttons
var rectwidth, rectheight; 

// variable for width of center showcase 
var centerwidth; 

// variable to determine pixel size of screen tiles 
var tile_size = 20; 

// x,y coordinates for respective elements 
var rect1x, rect1y; // left button
var rect2x, rect2y; // right button
var centerx, centery; // center showcase 
var imglx, imgly; // left img
var imgrx, imgry; // right img 
var imgcx, imgcy; // center img 

// projectimgs contains images for respective projects 
var projectimgs = [];

// project words contains bios for respective projects 
var projectwords = []; 

// tracks the project currently being viewed 
var index = 0;

// variable to hold the current bio to display 
var currimg, currtext; 
// contains the current heading to display 
var headingtext;
// contains the link to the current code repository 
var currlink;  

// array of circles
var circ_arr = []; 

// velocity for animations 
var anim_velocity = 10; 
var anim_out = false; 
var anim_in = false; 
var left_click = false; 
var right_click = false; 

// class for background elements 
class Circle {
  constructor(x, y, vely, velx, circ) {
    this.x = x;
    this.y = y;
    this.vely = vely; 
    this.velx = velx; 
    this.circ = circ; 
  }
}


// function determines the size of canvas to use 
function set_dims() {
  rectwidth = 80; 
  rectheight = 560; 
  centerwidth = 560;
  canvasWidth = 1120; 
  canvasHeight = 720;  
}


// function creates the image and text elements for each project and adds them to respective arrays 
function create_projects() {
  nnimg = loadImage("javascript/images/nn.png"); 
  nntext = "I coded a Neural Network entirely from scratch in C++. The model implements a simple feedforward network that uses logistic regression. Click to check out the project repo!"; 
  append(projectimgs, nnimg); 
  append(projectwords, nntext); 

  webimg = loadImage("javascript/images/webdev.png"); 
  webtext = "For this website, I used many different techniques for implementing the features you see in front of you. Most prominent in my development was the use of p5.js. Check out how it worked here!"; 
  append(projectimgs, webimg); 
  append(projectwords, webtext); 

  starwarsimg = loadImage("javascript/images/starwarsmodel.png"); 
  starwarstext = "I made a simple Convolutional Neural Network using Tensorflow that can classify images of different Star Wars characters including Darth Vader, Anakin, and Chewbacca. Check out the repo here!"; 
  append(projectimgs, starwarsimg); 
  append(projectwords, starwarstext);
}

// sets dimensions, creates projects, and loads all images necessary for displaying 
function preload() {

  set_dims(); 
  create_projects(); 

  arrow_img_left = loadImage("javascript/images/arrow_left.png");
  arrow_img_right = loadImage("javascript/images/arrow_right.png");
}

// determins the x and y coordinates for each respective element on the canvas 
function setup() {
  
  // sets left button on block 1 / 4 (12 / 48)
  // sets top margin on block 4 (4 / 36)
  rect1x = (tile_size * 14) - (rectwidth * 2);
  rect1y = (tile_size * 4);  
  
  // sets right button on block 3 / 4 (36 / 48)
  // sets top margin on block 4 (4 / 36)
  rect2x = (tile_size * 42) + 80; 
  rect2y = (tile_size * 4); 
  
  // sets center button on block 12 ( 12 / 48)
  // maintains same margin as left and right buttons 
  centerx = (tile_size * 14); 
  centery = rect1y; 
  
  // left image is 8 pixels to right of left button
  // left margin is 30 less than (height / 2) of the left button plus the top margin
  imglx = rect1x + 8; 
  imgly = ((rectheight / 2) - 30) + rect1y;
  
  // right image is 8 pixels to right of right button
  // right margin is calculated same way as the left margin
  imgrx = rect2x + 8; 
  imgry = ((rectheight / 2) - 30) + rect2y; 

  // center image is 20 pixels to right of center showcase 
  // center margin is 20 pixels off the top of center showcase 
  imgcx = (tile_size * 14) + 20; 
  imgcy = rect1y + 20; 

  // create array of background elements 
  for (var i = 0; i < 25; i++) {
    circlex = random(100, canvasWidth - 100); 
    circley = random(100, canvasHeight - 100); 
    velx = random(-2, 2); 
    vely = random(-2, 2); 
    circ = random(25, 50); 
    var circle = new Circle(circlex, circley, velx, vely, circ); 
    append(circ_arr, circle); 
  }
    
  createCanvas(canvasWidth, canvasHeight);
}

// sets the current image, text, and link to corresponding project being displayed 
function set_center() {
  currimg = projectimgs[index]; 
  currtext = projectwords[index];
  var proj; 
  if (index == 0) { 
    proj = ": Neural_Net.CPP"; 
    currlink = "https://codeberg.org/colinmcgravey/NeuralNet_CPP"; 
  }
  if (index == 1) {
    proj = ": Personal Website"; 
    currlink = "https://codeberg.org/colinmcgravey/pages";
  }
  if(index == 2) {
    proj = ": Star Wars Classifier";
    currlink = "https://codeberg.org/colinmcgravey/StarWars_Classifier"; 
  }
  headingtext = "Project " + (index + 1) + proj;  
}

// determines if user is pressing mouse within left, right, or center buttons 
function mousePressed() {
  if (centerx < mouseX && mouseX < centerx + centerwidth) {
    if (centery < mouseY && mouseY < rectheight + centery) {
      window.open(currlink); 
    }
  } 
  else if (rect1x < mouseX && mouseX < rect1x + rectwidth) {
    if (rect1y < mouseY && mouseY < rectheight + rect1y) {
      anim_out = true; 
      left_click = true; 
    }
  }
  else if (rect2x < mouseX && mouseX < rect2x + rectwidth) {
    if (rect2y < mouseY && mouseY < rectheight + rect2y) {
      anim_out = true; 
      right_click = true; 
    }
  }

}

// handles animation of the center showcase
function move_center() {
  if (anim_out) {
    centery += anim_velocity; 
    imgcy += anim_velocity; 
    if (centery > canvasHeight) {
      anim_out = false; 
      anim_in = true; 
      centery = rect1y - canvasHeight; 
      imgcy = rect1y - canvasHeight + 20;
      if (left_click) { 
        index -=1; 
        if (index < 0) { index = 2; }
        left_click = false; 
      }
      if (right_click) {
        index += 1; 
        if (index > 2) { index = 0; }
        right_click = false; 
      }
      
    }
  }
  if (anim_in) {
    centery += anim_velocity; 
    imgcy += anim_velocity; 
    if (centery >= rect1y) {
      anim_in = false; 
    }
  }
}

// loops through array of background elements, moving each circle and checking for collisions
function move_circles() {

  for (var i = 0; i < circ_arr.length; i++) {
    circ_arr[i].x += circ_arr[i].velx; 
    circ_arr[i].y += circ_arr[i].vely; 
    if ((circ_arr[i].x + (circ_arr[i].circ / 2)) > canvasWidth || (circ_arr[i].x - (circ_arr[i].circ / 2)) < 0) {
      circ_arr[i].velx = circ_arr[i].velx * -1; 
    }
    if ((circ_arr[i].y + (circ_arr[i].circ / 2)) > canvasHeight || (circ_arr[i].y - (circ_arr[i].circ / 2)) < 0) {
      circ_arr[i].vely = circ_arr[i].vely * -1; 
    }

  }

}

// main display loop, draws shapes, images, and displays current project 
function draw() {
  background("#92E5C6");
  set_center(); 
  fill("#AFB7F7");
  stroke(0); 
  for (var i = 0; i < circ_arr.length; i++){
    ellipse(circ_arr[i].x, circ_arr[i].y, circ_arr[i].circ); 
  } 
  move_circles(); 
  fill(255); 
  stroke(0);
  rect(rect1x, rect1y, rectwidth, rectheight); 
  rect(rect2x, rect2y, rectwidth, rectheight); 
  rect(centerx, centery, centerwidth, rectheight);
  image(arrow_img_left, imglx, imgly, arrow_img_left.height / 8, arrow_img_left.width / 8);
  image(arrow_img_right, imgrx, imgry, arrow_img_right.height / 8, arrow_img_right.width / 8);   
  image(currimg, imgcx, imgcy, centerwidth - 40, rectheight - 300); 
  textSize(30); 
  fill(0); 
  text(headingtext, imgcx, imgcy + 300); 
  textSize(20); 
  text(currtext, imgcx, imgcy + 340, centerwidth - 50); 
  move_center(); 

  if (rect1x < mouseX && mouseX < rect1x + rectwidth) {
    if (rect1y < mouseY && mouseY < rectheight + rect1y) {
      cursor(HAND);  
    }
  }
  else if (rect2x < mouseX && mouseX < rect2x + rectwidth) {
    if (rect2y < mouseY && mouseY < rectheight + rect2y) {
      cursor(HAND);  
    }
  }
  else if (centerx < mouseX && mouseX < centerx + centerwidth) {
    if (centery < mouseY && mouseY < rectheight + centery) {
      cursor(HAND);  
    }
  } 
  else {
    cursor(ARROW); 
  }


}