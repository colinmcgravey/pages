### Personal Website ###

This is the repository for all source code used to create my personal site. 
The link is available on my profile here. All animations for this project 
were created using p5.js, the code is contained within the javascript 
folder. The remainder of the layout of the site was created from scratch
using an HTML and CSS skeleton. 